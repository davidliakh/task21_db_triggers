#table for errors
CREATE TABLE IF NOT EXISTS error_msg (error_msg VARCHAR(32) NOT NULL PRIMARY KEY);
INSERT INTO error_msg VALUES ('Foreign Key Constraint Violated!');
INSERT INTO error_msg VALUES ('Forbiden!');

#trigger for insert employee
DELIMITER //
CREATE TRIGGER insert_employee
  BEFORE INSERT
  ON employee
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM post WHERE post=new.post)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF new.identity_number rlike '.+00$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
 END //
DELIMITER ;
#trigger for update employee
DELIMITER //
CREATE TRIGGER update_employee 
BEFORE UPDATE
ON employee
FOR EACH ROW
 BEGIN
    IF (SELECT COUNT(*) FROM post WHERE post=new.post)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF new.identity_number rlike '.+00$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;
#TRIGGER for insert pharmacy
DELIMITER //
CREATE TRIGGER insert_pharmacy
BEFORE INSERT
on pharmacy
FOR EACH ROW
BEGIN
IF (SELECT COUNT(*) FROM street WHERE street=new.street)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#trigger for update pharmacy
DELIMITER //
CREATE TRIGGER update_pharmacy
BEFORE UPDATE
on pharmacy
for each row 
 BEGIN
    IF (SELECT COUNT(*) FROM street WHERE street=new.street)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#TRIGGER for after update pharmacy
DELIMITER //
CREATE TRIGGER after_update_pharmacy
AFTER UPDATE
on pharmacy 
for each row
begin
	if new.id != old.id
    then
    update pharmacy_medicine SET pharmacy_id=new.id WHERE
    pharmacy_id=old.id;
		update employee SET pharmacy_id=new.id WHERE
        pharmacy_id=old.id;
        end if;
	end //
delimiter ;
# TRIGGER for  insert in medicine
delimiter //
create trigger insert_medicine
before insert
on medicine
for each row
begin
	if new.ministry_code not rlike '^[^МП]{2}-[0-9]{3}-[0-9]{2}$'
    then 
    INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    end if;
end //
delimiter ;
#trigger for update medicine

delimiter //
CREATE TRIGGER update_medicine
	before update
    on medicine
    for each row
    begin
     IF new.ministry_code not rlike '^[^МП]{2}-[0-9]{3}-[0-9]{2}$'
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#triggers for insert pharmacy_medicine
delimiter //
create trigger before_insert_pharmacy_mediccine
before insert
on pharmacy_medicine
for each row
BEGIN
    IF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
    IF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;

#triggers for update pharmacy_medicine
delimiter //
create trigger update_pharmacy_mediccine
before update
on pharmacy_medicine
for each row
BEGIN
    IF (SELECT COUNT(*) FROM pharmacy WHERE id=new.pharmacy_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
    IF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;
delimiter //
#trigger for insert in medicine_zone
	create trigger insert_medicine_zone
	before insert
    on medicine_zone
    for each row
    BEGIN
IF (SELECT COUNT(*) FROM zone WHERE id=new.zone_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;
#trigger for update medicine zone
delimiter //
	create trigger update_medicine_zone
	before update
    on medicine_zone
    for each row
    BEGIN
IF (SELECT COUNT(*) FROM zone WHERE id=new.zone_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    ELSEIF (SELECT COUNT(*) FROM medicine WHERE id=new.medicine_id)= 0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
END //
DELIMITER ;
#triger for update_after_medicine
DELIMITER //
CREATE TRIGGER update_after_medicine
  AFTER UPDATE
  ON medicine
  FOR EACH ROW
  BEGIN
    IF new.id != old.id 
    THEN 
		UPDATE pharmacy_medicine SET medicine_id=new.id WHERE 
		medicine_id=old.id;
        UPDATE medicine_zone SET medicine_id=new.id WHERE 
		medicine_id=old.id;
	END IF;
  END //
DELIMITER ;

#trigger for delete from post
DELIMITER //
CREATE TRIGGER delete_post
  BEFORE DELETE
  ON post
  FOR EACH ROW
  BEGIN
    INSERT error_msg VALUES ('Forbiden!');
  END //
DELIMITER ;
#trigger for delete from street
DELIMITER //
CREATE TRIGGER delete_street
  BEFORE DELETE
  ON street
  FOR EACH ROW
  BEGIN
      INSERT error_msg VALUES ('Forbiden!');
  END //
DELIMITER ;
#trigger for delete from zone
DELIMITER //
CREATE TRIGGER delete_zone
  BEFORE DELETE
  ON zone
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM medicine_zone WHERE zone_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;
#trigger for delete from pharmacy
DELIMITER //
CREATE TRIGGER delete_pharmacy
  BEFORE DELETE
  ON pharmacy
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM employee WHERE pharmacy_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF(SELECT COUNT(*) FROM pharmacy_medicine WHERE pharmacy_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;
#trigger for delete from medicine
DELIMITER //
CREATE TRIGGER delete_medicine
  BEFORE DELETE
  ON medicine
  FOR EACH ROW
  BEGIN
    IF (SELECT COUNT(*) FROM medicine_zone WHERE medicine_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
	ELSEIF(SELECT COUNT(*) FROM pharmacy_medicine WHERE medicine_id=old.id)=0
    THEN
      INSERT error_msg VALUES ('Foreign Key Constraint Violated!');
    END IF;
  END //
DELIMITER ;

#/////
#second task
 DELIMITER //
 CREATE TRIGGER check_identity_number
	BEFORE INSERT 
    ON employee for each row
BEGIN
	IF(NEW.identity_number RLIKE '0{2}$')
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'identity number can not have two zero in the end ' ;
	END IF;
END //
DELIMITER ;
#/////
#third task

 DELIMITER //
 CREATE TRIGGER ministry_code_check
	BEFORE INSERT 
    ON medicine for each row
BEGIN
	IF(NEW.ministry_code NOT RLIKE '^[А-Я]{2}-[0-9]{3}-[0-9]{2}')
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'can not insert this data' ;
	END IF;
END //
DELIMITER ;
#/////
#fourth task
 DELIMITER //
 CREATE TRIGGER ban_update
	BEFORE UPDATE 
    ON post
    THEN SIGNAL SQLSTATE '45000'
		SET MESSAGE_TEXT = 'can not update this table' ;
DELIMITER ;
 








    





		
	



